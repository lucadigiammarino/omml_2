import numpy as np
import scipy as sp
import cvxopt as cvx
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.utils import shuffle
import itertools
import time
import operator

trainSet2 = np.genfromtxt("./datasets/Train_2.csv", delimiter = ',', skip_header = 1, usecols = range(1, 257))
trainSet8 = np.genfromtxt("./datasets/Train_8.csv", delimiter = ',', skip_header = 1, usecols = range(1, 257))
testSet2  = np.genfromtxt("./datasets/Test_2.csv",  delimiter = ',', skip_header = 1, usecols = range(1, 257))
testSet8  = np.genfromtxt("./datasets/Test_8.csv",  delimiter = ',', skip_header = 1, usecols = range(1, 257))

X_train = np.concatenate([trainSet2, trainSet8], 0)
X_test  = np.concatenate([testSet2, testSet8], 0)

trainLabel2 = np.ones(len(trainSet2))
trainLabel8 = np.ones(len(trainSet8))* -1
testLabel2  = np.ones(len(testSet2))
testLabel8  = np.ones(len(testSet8)) * -1

Y_train = np.concatenate([trainLabel2, trainLabel8], 0)
Y_test  = np.concatenate([testLabel2,   testLabel8], 0)

X_train, Y_train = shuffle(X_train, Y_train, random_state = 23)

N_samples_train = len(X_train) # total number of parameters
N_samples_test  = len(X_test)  # total number of parameters
N_feat          = len(X_train[0])

Y_matrix = np.eye(N_samples_train) * Y_train # diagonal matrix filled by the Y values


def pol_kernel(x, y, p):

    kernel = (np.matmul(x, y.T) + 1) ** p

    return kernel

def working_sets(alpha, y, c, epsilon):

    L_p = []
    U_p = []
    L_m = []
    U_m = []
    F   = []

    for a in range(len(alpha)):

        if  (alpha[a] <=     epsilon and y[a] ==  1): L_p.append(a)
        elif(alpha[a] <=     epsilon and y[a] == -1): L_m.append(a)
        elif(alpha[a] >= c - epsilon and y[a] ==  1): U_p.append(a)
        elif(alpha[a] >= c - epsilon and y[a] == -1): U_m.append(a)

        else: F.append(a)

    R_alpha = L_p + U_m + F
    S_alpha = L_m + U_p + F

    return R_alpha, S_alpha


def determine_t(di, dj, alphai, alphaj, c):

    if (di > 0 and dj > 0):   t = min(c - alphai, c - alphaj)
    elif (di < 0 and dj < 0): t = min(alphai, alphaj)
    elif (di > 0 and dj < 0): t = min(c - alphai, alphaj)
    elif (di < 0 and dj > 0): t = min(alphai, c - alphaj)

    return t

def support_vector(x, y, alpha, feat, treshold):
    
    sv = 0

    for a in range(len(alpha)):

        if (alpha[a] < treshold): alpha[a] = treshold
        else: sv += 1
    
    new_x     = np.zeros((sv, feat))
    new_y     = np.zeros(sv)
    new_alpha = np.zeros(sv)
    s         = 0
    
    for i in range(len(alpha)):

        if (alpha[i] > treshold):

            new_x[s, :]  = x[i, :]
            new_y[s]     = y[i]
            new_alpha[s] = alpha[i]

            s += 1

    return new_x, new_y, new_alpha, sv

def optimal_parameters(alpha, x, y, p, N):

    if (N == 0): N = 1

    kernel_x = pol_kernel(x, x, p)
    b        = (1 - np.dot(alpha * y, kernel_x))
    b_star   = (np.sum(b, 0) / float(N))

    return b_star

def objective_function(alpha, Q, e):
    
    return (np.dot(np.dot(alpha.T, Q), alpha) - np.dot(e.T, alpha)) / 2

def decision_function(alpha, x, xi, y, p, b):

    kernel_x = pol_kernel(xi, x, p)
    f        = np.sign(np.dot(alpha * y, kernel_x) + b)

    return f

# Performance evaluation
def accuracy(trained, truth_values, N):

    score = 0
    res   = trained * truth_values

    for y in res:
        
        if (np.sign(y) > 0): score += 1

    return score / N

def calculate_gradient(gradient, y):

    y      = np.reshape(y, (len(y), 1))
    grad_y = - y * gradient

    return grad_y
# --------------- Parameters ---------------#

C          = 2**-3     # parameter used for constraints definition
p_exp      = 1         # polynomial kernel, exponent
gamma      = 0.1       # RBF kernel, gamma
tresholdSV = 10 ** -11
tresholdB  = 1e-3

phi_x_pol = pol_kernel(X_train, X_train, p_exp) # Polynomial kernel

e     = np.ones((N_samples_train, 1))
alpha = np.ones(N_samples_train)*(10**-4)


# --------------- Main CVXOPT approach ---------------#

time_start = time.time()

Q          = np.dot(np.dot(Y_matrix, phi_x_pol),Y_matrix)
grad       = -e
grad_y     = np.zeros((2,N_samples_train))
prev_obj_f = objective_function(alpha, Q, e)

for iteration in range (2000):
    
    directions  = np.zeros(N_samples_train)
    old_alpha   = alpha
    grad_y[0,:] = np.reshape(calculate_gradient(grad, Y_train), (N_samples_train))
    grad_y[1,:] = np.array(np.arange(N_samples_train))
    R, S        = working_sets(alpha, Y_train, C, tresholdB)
    grad_R      = np.zeros((2,len(R)))
    grad_S      = np.zeros((2,len(S)))

    r = 0
    s = 0
    
    for i in range(len(grad_y[1])):
    
        if i in R:
            grad_R[:, r] = grad_y[:, i]
            
            r += 1
        
        elif i in S:
        
            grad_S[:, s] = grad_y[:, i]
        
            s += 1
  
    grad_R_ord = grad_R[:, grad_R[0, :].argsort()]
    grad_S_ord = grad_S[:, grad_S[0, :].argsort()]

    m = grad_S_ord[1][0]
    M = grad_R_ord[1][len(R) - 1]
    J = int(m)
    I = int(M)

    objective_f = objective_function(alpha, Q, e)
    
    if(m - M < 10**-2 and objective_f < 0):
        print("KKT satisfied")
        break

    prev_obj_f    = objective_f
    directions[J] = -Y_train[J]
    directions[I] =  Y_train[I]

    t_numerator = np.dot(grad.T, directions)
    
    if t_numerator > 0: directions = -directions
    
    if np.dot(np.dot(directions.T, Q), directions) > 0: t_un= -np.dot(grad.T, directions) / np.dot(np.dot(directions.T, Q), directions)
    else: t_un = float("+inf")
    
    dI     = directions[I]
    dJ     = directions[J]
    t_bar  = determine_t(directions[I], directions[J], alpha[I], alpha[J], C)
    t_star = min(t_un, t_bar)

    # Compute new alpha[J]
    alpha[J] = alpha[J] + (t_star * directions[J])
    
    if(alpha[J] < tresholdB):       alpha[J] = tresholdB
    elif(alpha[J] > C - tresholdB): alpha[J] = C

    # #Compute new alpha[I]
    alpha[I] = alpha[I] + (t_star * directions[I])
    
    if(alpha[I]< tresholdB):      alpha[I] = tresholdB
    elif(alpha[I] > C-tresholdB): alpha[I] = C

    grad = grad + t_star * np.reshape((Q[:, I] + Q[:, J]), (N_samples_train, 1))

objective_f = objective_function(alpha, Q, e)

X_sv, Y_sv, alpha_sv, N_sv = support_vector(X_train, Y_train, alpha, N_feat, tresholdSV)
opt_b                      = optimal_parameters(alpha_sv, X_sv, Y_sv, p_exp, N_sv)


training_computing_time = time.time() - time_start

Y_predicted_train = decision_function(alpha_sv, X_train, X_sv, Y_sv, p_exp, opt_b)
accuracy_train    = accuracy(Y_predicted_train, Y_train, N_samples_train)

Y_predicted_test = decision_function(alpha_sv, X_test, X_sv, Y_sv, p_exp, opt_b)
accuracy_test    = accuracy(Y_predicted_test, Y_test, N_samples_test)

print("Objective function before eval: ", objective_function(np.ones(N_samples_train)*(10**-4), Q, e)[0])
print("Objective function after eval: ", objective_f[0])

print("############################################################")

print("Classification rate on training set: ", accuracy_train*100, "%")
print("Classification rate on testing set: ", accuracy_test*100, "%")
print("Time for finding KKT point: ", training_computing_time, "s")
print("Number of optimization iterations: ", iteration)
print("Value of gamma null: ", "### Value of p: ", p_exp)

print("############################################################")

print("Difference between m(alpha) and M(alpha): ", m - M)
