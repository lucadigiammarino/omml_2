import numpy as np
import cvxopt as cvx
from sklearn.utils import shuffle
import time

#---------------- Functions ---------------#

# Solve the SVM training dual problem using CVXOPT library
# the dual problem is a QP problem in the form: (1/2)*alpha*Q*alpha + e*alpha
# with constraints: alpha*yval = b,
#                   0 <= alpha[i] <= self.C
#
# return: alpha*
def solve_dual(P, e, G, constraint, Y, b, treshold):
	
    Q  = cvx.matrix(P)
    e  = cvx.matrix(e)
    G  = cvx.matrix(G)
    h  = cvx.matrix(constraint)
    A  = cvx.matrix(np.array([Y]))
    b  = cvx.matrix(np.full((1, 1), float(b)))

    cvx.solvers.options['maxiters'] = 50

    res = cvx.solvers.qp(Q, e, G, h, A, b)

    return np.array(res['x']).flatten(), res['iterations'], 0, 0, res['primal objective']


def support_vector(x, y, alpha, feat):
	
    sv = 0

    for a in range(len(alpha)):
		
        if (alpha[a] < treshold): alpha[a] = 0
        else: sv += 1

    new_x     = np.zeros((sv, feat))
    new_y     = np.zeros(sv)
    new_alpha = np.zeros(sv)
    s         = 0
	
    for i in range(len(alpha)):
	
        if (alpha[i] != 0):
	
            new_x[s,:]   = x[i,:]
            new_y[s]     = y[i]
            new_alpha[s] = alpha[i]
			
            s += 1
	
    return new_x, new_y, new_alpha, sv

# Compute optimal b after alpha* was found, using als W*
def optimal_parameters(alpha, x, y, p, N):

    if(N == 0): N = 1

    kernel_x = pol_kernel(x, x, p)
    b        = (1 - np.dot(alpha * y, kernel_x))
    b_star   = (np.sum(b, 0) / float(N))
	
    return b_star

# Function used to predict the value of Y
def decision_function(alpha, x, xi, y, p, b):
	
    kernel_x = pol_kernel(xi, x, p)
    
    f = np.sign(np.dot(alpha * y, kernel_x) + b)
    d = np.dot(alpha * y, kernel_x) + b

    return f, d

# Performance evaluation
def accuracy(trained, truth_values, N):

    score = 0

    for y in truth_values:
    
        if (trained[int(y)] == truth_values[int(y)]): score += 1
	
    return score / N
    
#--------------- Kernels ---------------#

# Polynomial kernel
def pol_kernel(x, y, p):

	kernel = (np.matmul(x, y.T) +1)**p

	return kernel

# RBF kernel
def rbf_kernel(x, y, gamma):
	
	x_norm = np.sum(x**2, axis = -1)
	y_norm = np.sum(y**2, axis = -1)
	kernel = np.exp(-gamma * (x_norm[:, None] + y_norm[None, :] - 2 * np.dot(x, y.T)))

	return kernel

# --------------- Open dataset ---------------#

trainSet1 = np.genfromtxt("./datasets/Train_1.csv", delimiter = ',', skip_header = 1, usecols = range(1, 257))
trainSet2 = np.genfromtxt("./datasets/Train_2.csv", delimiter = ',', skip_header = 1, usecols = range(1, 257))
trainSet8 = np.genfromtxt("./datasets/Train_8.csv", delimiter = ',', skip_header = 1, usecols = range(1, 257))
testSet1  = np.genfromtxt("./datasets/Test_1.csv",  delimiter = ',', skip_header = 1, usecols = range(1, 257))
testSet2  = np.genfromtxt("./datasets/Test_2.csv",  delimiter = ',', skip_header = 1, usecols = range(1, 257))
testSet8  = np.genfromtxt("./datasets/Test_8.csv",  delimiter = ',', skip_header = 1, usecols = range(1, 257))

X_train = np.concatenate([trainSet1, trainSet2, trainSet8], 0)
X_test  = np.concatenate([testSet1,   testSet2,  testSet8], 0)

trainLabel1 = np.ones(len(trainSet1))
trainLabel2 = np.ones(len(trainSet2))
trainLabel8 = np.ones(len(trainSet8))

Y_train_1_28 = np.concatenate([trainLabel1, -trainLabel2, -trainLabel8], 0)
Y_train_2_18 = np.concatenate([-trainLabel1, trainLabel2, -trainLabel8], 0)
Y_train_8_12 = np.concatenate([-trainLabel1, -trainLabel2, trainLabel8], 0)

ground_truth_train = np.concatenate([trainLabel1 * 0, trainLabel2 * 1, trainLabel8 * 2], 0)

testLabel1 = np.ones(len(testSet1))
testLabel2 = np.ones(len(testSet2))
testLabel8 = np.ones(len(testSet8))

Y_test_1_28 = np.concatenate([testLabel1, -testLabel2, -testLabel8], 0)
Y_test_2_18 = np.concatenate([-testLabel1, testLabel2, -testLabel8], 0)
Y_test_8_12 = np.concatenate([-testLabel1, -testLabel2, testLabel8], 0)

ground_truth_test = np.concatenate([testLabel1 * 0, testLabel2 * 1, testLabel8 * 2], 0)

X_train, Y_train_1_28, Y_train_2_18, Y_train_8_12, ground_truth_train  = shuffle(X_train, Y_train_1_28, Y_train_2_18, Y_train_8_12, ground_truth_train, random_state = 1811286)
X_test, Y_test_1_28, Y_test_2_18, Y_test_8_12, ground_truth_test       = shuffle(X_test, Y_test_1_28, Y_test_2_18, Y_test_8_12, ground_truth_test, random_state = 1811286)

N_samples_train = len(X_train) # total number of parameters
N_samples_test  = len(X_test)  # total number of parameters

Y_matrix_1 = np.eye(N_samples_train) * Y_train_1_28 # diagonal matrix filled by the Y values
Y_matrix_2 = np.eye(N_samples_train) * Y_train_2_18 # diagonal matrix filled by the Y values
Y_matrix_8 = np.eye(N_samples_train) * Y_train_8_12 # diagonal matrix filled by the Y values

# --------------- Parameters ---------------#

C            = 2**-3                            # parameter used for constraints definition    
lower_bounds = np.zeros((N_samples_train, 1))   # alpha >= 0
upper_bounds = np.full((N_samples_train, 1), C) # alpha <= C
p_exp        = 1                                # polynomial kernel, exponent
gamma        = 0.1                              # RBF kernel, gamma
treshold	 = 10**-11
N_feat	     = len(X_train[0])
alpha_0      = np.zeros(N_samples_train)

kernel_x_pol = pol_kernel(X_train, X_train, p_exp) # Polynomial kernel
kernel_x_RBF = rbf_kernel(X_train, X_train, gamma) # RBF kernel

P1 =  np.dot((np.dot(Y_matrix_1, kernel_x_pol)), Y_matrix_1)
P2 =  np.dot((np.dot(Y_matrix_2, kernel_x_pol)), Y_matrix_2)
P8 =  np.dot((np.dot(Y_matrix_8, kernel_x_pol)), Y_matrix_8)
b  =  np.random.rand(N_samples_train,1)  
h  =  np.concatenate([lower_bounds, upper_bounds], 0)
e  = -np.ones((N_samples_train,1))
Gp =  np.eye(N_samples_train)
Gn = -np.eye(N_samples_train)
G  =  np.concatenate([Gn,Gp], 0)

cvx.solvers.options["show_progress"] = False

#--------------- Main CVXOPT ---------------#

time_start  = time.time()
obj_fun     = np.dot(np.dot(alpha_0.T, P1), alpha_0) - np.dot(e.T, alpha_0)

# 1-against-28
opt_alpha, iter1, _, _, fprime1  = solve_dual(P1, e, G, h, Y_train_1_28, 0, treshold)
X_sv_1, Y_sv_1, alpha_sv_1, N_sv = support_vector(X_train, Y_train_1_28, opt_alpha, N_feat)
opt_b_1                          = optimal_parameters(alpha_sv_1, X_sv_1, Y_sv_1, p_exp, N_sv)
Y_predicted_train_1, dist_1      = decision_function(alpha_sv_1, X_train, X_sv_1, Y_sv_1, p_exp, opt_b_1)

# 2-against-18
opt_alpha, iter2, _, _, fprime2  = solve_dual(P2, e, G, h, Y_train_2_18, 0, treshold)
X_sv_2, Y_sv_2, alpha_sv_2, N_sv = support_vector(X_train, Y_train_2_18, opt_alpha, N_feat)
opt_b_2                          = optimal_parameters(alpha_sv_2, X_sv_2, Y_sv_2, p_exp, N_sv)
Y_predicted_train_2, dist_2      = decision_function(alpha_sv_2, X_train, X_sv_2, Y_sv_2, p_exp, opt_b_2)

# 8-against-12
opt_alpha, iter8, _, _, fprim8   = solve_dual(P8, e, G, h, Y_train_8_12, 0, treshold)
X_sv_8, Y_sv_8, alpha_sv_8, N_sv = support_vector(X_train, Y_train_8_12, opt_alpha, N_feat)
opt_b_8                          = optimal_parameters(alpha_sv_8, X_sv_8, Y_sv_8, p_exp, N_sv)
Y_predicted_train_8, dist_8      = decision_function(alpha_sv_8, X_train, X_sv_8, Y_sv_8, p_exp, opt_b_8)

j_train_star   = np.argmax([dist_1, dist_2, dist_8], 0)
accuracy_train = accuracy(j_train_star, ground_truth_train, N_samples_train)

Y_predicted_test_1, dist_test1 = decision_function(alpha_sv_1, X_test, X_sv_1, Y_sv_1, p_exp, opt_b_1)
Y_predicted_test_2, dist_test2 = decision_function(alpha_sv_2, X_test, X_sv_2, Y_sv_2, p_exp, opt_b_2)
Y_predicted_test_8, dist_test8 = decision_function(alpha_sv_8, X_test, X_sv_8, Y_sv_8, p_exp, opt_b_8)

j_test_star   = np.argmax([dist_test1, dist_test2, dist_test8], 0)
accuracy_test = accuracy(j_test_star, ground_truth_test, N_samples_test)

training_computing_time = time.time() - time_start

print("Objective function before eval: ", obj_fun[0])
print("Objective function after eval: ", fprime1 + fprime2 + fprim8)

print("############################################################")

print("Classification rate on training set: ", accuracy_train * 100, "%")
print("Classification rate on testing set: ", accuracy_test * 100, "%")
print("Time for finding KKT point: ", training_computing_time, "s")
print("Number of optimization iterations: ", iter1 + iter2 + iter8)
print("Value of gamma null: ", "### Value of p: ", p_exp)