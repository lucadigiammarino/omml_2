import numpy as np
import scipy as sp
import cvxopt as cvx
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.utils import shuffle
import itertools
import time

#---------------- Functions ---------------#

# Solve the SVM training dual problem using CVXOPT library
# the dual problem is a QP problem in the form: (1/2)*alpha*Q*alpha + e*alpha
# with constraints: alpha*yval = b,
#                   0 <= alpha[i] <= self.C
#
# return: alpha*
def solve_dual(P, e, G, constraint, Y, b, treshold):

    Q = cvx.matrix(P)
    e = cvx.matrix(e)
    G = cvx.matrix(G)
    h = cvx.matrix(constraint)
    A = cvx.matrix(np.array([Y]))
    b = cvx.matrix(np.full((1, 1), float(b)))

    cvx.solvers.options['maxiters'] = 50

    res = cvx.solvers.qp(Q, e, G, h, A, b)

    return np.array(res['x']).flatten(), res['iterations'], 0, 0, res['primal objective']


def support_vector(x, y, alpha, feat):
   
    sv = 0

    for a in range(len(alpha)):

        if (alpha[a] < treshold):
            alpha[a] = treshold
        else:
            sv += 1

    new_x     = np.zeros((sv, feat))
    new_y     = np.zeros(sv)
    new_alpha = np.zeros(sv)
    s         = 0

    for i in range(len(alpha)):

        if (alpha[i] != treshold):
           
            new_x[s, :]  = x[i, :]
            new_y[s]     = y[i]
            new_alpha[s] = alpha[i]

            s += 1

    return new_x, new_y, new_alpha, sv

# Compute optimal b after alpha* was found, using als W*
def optimal_parameters(alpha, x, y, p, N):

	kernel_x = pol_kernel(x,x,p)
	b        = (1 - np.dot(alpha * y, kernel_x))
	b_star   =(np.sum(b, 0) / float(N))
	
	return b_star

# Function used to predict the value of Y
def decision_function(alpha, x, xi, y, p, b):
	
    kernel_x = pol_kernel(xi, x, p)
    f        = np.sign(np.dot(alpha * y, kernel_x) + b)

    return f

# Performance evaluation
def accuracy(trained, truth_values, N):

	score = 0
	res   = trained * truth_values
	
	for y in res:

		if (np.sign(y) > 0): score += 1
	
	return score / N
	
#--------------- Kernels ---------------#

# Polynomial kernel
def pol_kernel(x, y, p):

	kernel = (np.matmul(x, y.T) + 1)**p

	return kernel

# RBF kernel
def rbf_kernel(x, y, gamma):
	
    x_norm = np.linalg.norm(x)**2
    y_norm = np.linalg.norm(y)**2
    kernel = np.exp(-gamma * (x_norm + y_norm - 2 * np.dot(x, y.T)))

    return kernel

#--------------- Open dataset ---------------#

trainSet2 = np.genfromtxt("./datasets/Train_2.csv", delimiter = ',', skip_header = 1, usecols = range(1,257))
trainSet8 = np.genfromtxt("./datasets/Train_8.csv", delimiter = ',', skip_header = 1, usecols = range(1,257))
testSet2  = np.genfromtxt("./datasets/Test_2.csv",  delimiter = ',', skip_header = 1, usecols = range(1,257))
testSet8  = np.genfromtxt("./datasets/Test_8.csv",  delimiter = ',', skip_header = 1, usecols = range(1,257))

X_train = np.concatenate([trainSet2, trainSet8], 0)
X_test  = np.concatenate([testSet2,   testSet8], 0)

trainLabel2 = np.ones(len(trainSet2))
trainLabel8 = np.ones(len(trainSet8))* -1
testLabel2  = np.ones(len(testSet2))
testLabel8  = np.ones(len(testSet8)) * -1

Y_train = np.concatenate([trainLabel2, trainLabel8], 0)
Y_test  = np.concatenate([testLabel2, testLabel8], 0)

X_train, Y_train = shuffle(X_train, Y_train, random_state = 1811286)


N_samples_train = len(X_train) # total number of parameters
N_samples_test  = len(X_test)  # total number of parameters

Y_matrix = np.eye(N_samples_train) * Y_train # diagonal matrix filled by the Y values

#--------------- Parameters ---------------#

p_exp        = 1                                # polynomial kernel, exponent
C            = 2**-3                            # parameter used for constraints definition
lower_bounds = np.zeros((N_samples_train, 1))   # alpha >= 0
upper_bounds = np.full((N_samples_train, 1), C) # alpha <= C
gamma        = 2                                # RBF kernel, gamma
treshold	 = 10**-11
N_feat	     = len(X_train[0])
opt_alpha    = np.ones((N_samples_train, 1))*(10**-3)

kernel_x_pol = pol_kernel(X_train, X_train, p_exp) # Polynomial kernel
kernel_x_RBF = rbf_kernel(X_train, X_train, gamma) # RBF kernel

P  =  np.dot((np.dot(Y_matrix, kernel_x_pol)), Y_matrix)
b  =  np.random.rand(N_samples_train,1)
h  =  np.concatenate([lower_bounds, upper_bounds], 0)
e  = -np.ones((N_samples_train,1))
Gp =  np.eye(N_samples_train)
Gn = -np.eye(N_samples_train)
G  =  np.concatenate([Gn,Gp], 0)

cvx.solvers.options["show_progress"] = False

#--------------- Main CVXOPT approach ---------------#

time_start                           = time.time()
obj_fun                              = np.dot(np.dot(opt_alpha.T, P), opt_alpha) - np.dot(e.T, opt_alpha)
opt_alpha, iterations, _, _, f_prime = solve_dual(P, e, G, h, Y_train, 0, treshold)
X_sv, Y_sv, alpha_sv, N_sv           = support_vector(X_train, Y_train, opt_alpha, N_feat)
opt_b                                = optimal_parameters(alpha_sv, X_sv, Y_sv, p_exp, N_sv)
training_computing_time              = time.time() - time_start

Y_predicted_train = decision_function(alpha_sv, X_train, X_sv, Y_sv, p_exp, opt_b)
accuracy_train    = accuracy(Y_predicted_train, Y_train, N_samples_train)

Y_predicted_test = decision_function(alpha_sv, X_test, X_sv, Y_sv, p_exp, opt_b)
accuracy_test    = accuracy(Y_predicted_test, Y_test, N_samples_test)

print("Objective function before eval: ", obj_fun[0][0])
print("Objective function after eval: ", f_prime)

print("############################################################")

print("Classification rate on training set: ", accuracy_train*100, "%")
print("Classification rate on testing set: ", accuracy_test*100, "%")
print("Time for finding KKT point: ", training_computing_time, "s")
print("Number of optimization iterations: ", iterations)
print("Value of gamma: null", "### Value of p: ", p_exp)