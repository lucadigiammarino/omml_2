import numpy as np
import scipy as sp
import cvxopt as cvx
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.utils import shuffle
import itertools
import time
import operator

# ---------------- Functions ---------------#

# Solve the SVM training dual problem using CVXOPT library
# the dual problem is a QP problem in the form: (1/2)*alpha*Q*alpha + e*alpha
# with constraints: alpha*yval = b,
#                   0 <= alpha[i] <= self.C
#
# return: alpha*
def solve_dual(P, e, G, constraint, Y, b):

    Q = cvx.matrix(P)
    e = cvx.matrix(e)
    G = cvx.matrix(G)
    h = cvx.matrix(constraint)
    A = cvx.matrix(np.array([Y]))
    b = cvx.matrix(np.full((1, 1), float(b)))

    cvx.solvers.options['maxiters'] = 50

    res = cvx.solvers.qp(Q, e, G, h, A, b)

    return np.array(res['x']).flatten(), res['iterations'], 0, 0, res['primal objective']


def support_vector(x, y, alpha, feat, treshold):
    
    sv = 0

    for a in range(len(alpha)):

        if (alpha[a] < treshold):
            alpha[a] = 0
        else:
            sv += 1

    new_x     = np.zeros((sv, feat))
    new_y     = np.zeros(sv)
    new_alpha = np.zeros(sv)
    s         = 0

    for i in range(len(alpha)):

        if (alpha[i] != 0):
           
            new_x[s, :]  = x[i, :]
            new_y[s]     = y[i]
            new_alpha[s] = alpha[i]

            s += 1

    return new_x, new_y, new_alpha, sv

def select_subset(P, x, y, alpha, W, p, N):

    x_w      = np.zeros((len(W), 256))
    y_w      = np.zeros(len(W))
    alpha_w  = np.zeros(len(W))
    alpha_n  = np.zeros(N - len(W))
    Q_n_temp = np.zeros((N, len(W)))
    Q_n      = np.zeros(((N-len(W)), len(W)))
    Qi       = np.zeros((len(W), len(alpha)))

    for i in range(len(W)):

        w             = int(W[i])
        x_w[i]        = x[w]
        y_w[i]        = y[w]
        alpha_w[i]    = alpha[w]
        Q_n_temp[:,i] = P[:, w]

    c = 0
    d = 0
    
    for i in range(N):
    
        if i not in W:
    
            Q_n[c,:]   = Q_n_temp[i,:]
            alpha_n[c] = alpha[i]
            c += 1
        else:
            
            Qi[d,:] = P[:, i]
            d += 1


    Q_w = np.dot((np.dot(np.diag(y_w), pol_kernel(x_w, x_w, p))), np.diag(y_w))
    e_w = np.dot(alpha_n, Q_n) - np.ones(len(W))

    return Q_w, alpha_w, e_w, y_w, Qi

# Compute optimal b after alpha* was found, using als W*
def optimal_parameters(alpha, x, y, p, N):

    if (N == 0): N = 1

    kernel_x = pol_kernel(x, x, p)
    b        = (1 - np.dot(alpha * y, kernel_x))
    b_star   = (np.sum(b, 0) / float(N))

    return b_star

# Function used to predict the value of Y
def decision_function(alpha, x, xi, y, p, b):

    kernel_x = pol_kernel(xi, x, p)
    f        = np.sign(np.dot(alpha * y, kernel_x) + b)

    return f

# Performance evaluation
def accuracy(trained, truth_values, N):

    score = 0
    res   = trained * truth_values

    for y in res:
        
        if (np.sign(y) > 0): score += 1

    return score / N

def working_sets(alpha, y, c, epsilon):

    L_p = []
    U_p = []
    L_m = []
    U_m = []
    F   = []

    for a in range(len(alpha)):

        if  (alpha[a] <=     epsilon and y[a] ==  1): L_p.append(a)
        elif(alpha[a] <=     epsilon and y[a] == -1): L_m.append(a)
        elif(alpha[a] >= c - epsilon and y[a] ==  1): U_p.append(a)
        elif(alpha[a] >= c - epsilon and y[a] == -1): U_m.append(a)

        else: F.append(a)

    R_alpha = L_p + U_m + F
    S_alpha = L_m + U_p + F

    return R_alpha, S_alpha


def calculate_gradient(gradient, y):

    y      = np.reshape(y, (len(y), 1))
    grad_y = - y * gradient

    return grad_y

# --------------- Kernels ---------------#

# Polynomial kernel
def pol_kernel(x, y, p):

    kernel = (np.matmul(x, y.T) + 1) ** p

    return kernel

# RBF kernel
def rbf_kernel(x, y, gamma):

    x_norm = np.sum(x ** 2, axis=-1)
    y_norm = np.sum(y ** 2, axis=-1)
    kernel = np.exp(-gamma * (x_norm[:, None] + y_norm[None, :] - 2 * np.dot(x, y.T)))

    return kernel

# --------------- Open dataset ---------------#

trainSet2 = np.genfromtxt("./datasets/Train_2.csv", delimiter=',', skip_header = 1, usecols = range(1, 257))
trainSet8 = np.genfromtxt("./datasets/Train_8.csv", delimiter=',', skip_header = 1, usecols = range(1, 257))
testSet2  = np.genfromtxt("./datasets/Test_2.csv", delimiter=',',  skip_header = 1, usecols = range(1, 257))
testSet8  = np.genfromtxt("./datasets/Test_8.csv", delimiter=',',  skip_header = 1, usecols = range(1, 257))

X_train = np.concatenate([trainSet2, trainSet8], 0)
X_test  = np.concatenate([testSet2,   testSet8], 0)

trainLabel2 = np.ones(len(trainSet2))
trainLabel8 = np.ones(len(trainSet8))* -1
testLabel2  = np.ones(len(testSet2))
testLabel8  = np.ones(len(testSet8)) * -1

Y_train = np.concatenate([trainLabel2, trainLabel8], 0)
Y_test  = np.concatenate([testLabel2,   testLabel8], 0)

X_train, Y_train = shuffle(X_train, Y_train, random_state = 1811286)

N_samples_train = len(X_train) # total number of parameters
N_samples_test  = len(X_test)  # total number of parameters

Y_matrix = np.eye(N_samples_train) * Y_train # diagonal matrix filled by the Y values

# --------------- Parameters ---------------#

q          = 100
p_exp      = 1                                # polynomial kernel, exponent
C          = 2**-3                            # parameter used for constraints definition
gamma      = 0.1  # RBF kernel, gamma
tresholdSV = 10 ** -11
tresholdB  = 1e-3
N_feat     = len(X_train[0])
phi_x_pol  = pol_kernel(X_train, X_train, p_exp)  # Polynomial kernel
phi_x_RBF  = rbf_kernel(X_train, X_train, gamma)  # RBF kernel

Pv =  np.dot((np.dot(Y_matrix, phi_x_pol)), Y_matrix)
Gp =  np.eye(q)
Gn = -np.eye(q)
G  =  np.concatenate([Gn, Gp], 0)
e  =  np.ones((N_samples_train, 1))

alpha_0      = np.ones(N_samples_train)*(10**-3)
opt_alpha    = alpha_0
lower_bounds = np.zeros((q, 1))   # alpha >= 0
upper_bounds = np.full((q, 1), C) # alpha <= C
h            = np.concatenate([lower_bounds, upper_bounds], 0)

cvx.solvers.options["show_progress"] = False

# --------------- Main CVXOPT approach ---------------#

time_start = time.time()

fprim_prev = 0
K          = np.dot(np.dot(Y_train, Y_train.T), np.dot(X_train, X_train.T))
grad_y     = np.zeros((2,N_samples_train))
grad       = -e
q2         = int(q/2)

for iterations in range(100):

    old_alpha   = opt_alpha
    grad_y[0,:] = np.reshape(calculate_gradient(grad, Y_train), (N_samples_train))
    grad_y[1,:] = np.array(np.arange(N_samples_train))
    R, S        = working_sets(opt_alpha, Y_train, C, tresholdB)
    grad_R      = np.zeros((2,len(R)))
    grad_S      = np.zeros((2,len(S)))
    
    r = 0
    s = 0
    
    for i in range(len(grad_y[1])):
        
        if i in R:
        
            grad_R[:,r] = grad_y[:,i]
        
            r += 1
        
        elif i in S:
        
            grad_S[:, s] = grad_y[:, i]
            
            s += 1
  
    grad_R_ord = grad_R[:, grad_R[0, :].argsort()]
    grad_S_ord = grad_S[:, grad_S[0, :].argsort()]

    index_m = grad_S_ord[1][:q2]
    index_M = grad_R_ord[1][len(R) - q2:]

    m = grad_S_ord[1][0]
    M = grad_R_ord[1][len(R) - 1]
    W = np.concatenate((index_M, index_m), 0)

    P_w, alpha_w, e_w, Y_train_w, Qi = select_subset(K, X_train, Y_train, opt_alpha, W, p_exp, N_samples_train)
    opt_alpha_w, iter, _, _, f_prime = solve_dual(P_w, e_w, G, h, Y_train_w, 0)
    
    for k in range(q):

        w            = int(W[k])
        opt_alpha[w] = opt_alpha_w[k]

    grad = grad + np.reshape(np.sum(Qi, axis = 0) * (opt_alpha - old_alpha), (N_samples_train, 1))

    if(m < M):
        break

    fprim_prev = f_prime

training_computing_time = time.time() - time_start

obj_fun = np.dot(np.dot(opt_alpha.T, K), opt_alpha) - np.dot(e.T, opt_alpha)

X_sv, Y_sv, alpha_sv, N_sv = support_vector(X_train, Y_train, opt_alpha, N_feat, tresholdSV)
opt_b                      = optimal_parameters(alpha_sv, X_sv, Y_sv, p_exp, N_sv)

Y_predicted_train = decision_function(alpha_sv, X_train, X_sv, Y_sv, p_exp, opt_b)
accuracy_train    = accuracy(Y_predicted_train, Y_train, N_samples_train)

Y_predicted_test = decision_function(alpha_sv, X_test, X_sv, Y_sv, p_exp, opt_b)
accuracy_test    = accuracy(Y_predicted_test, Y_test, N_samples_test)

print("Objective function before eval", obj_fun[0])
print("Objective function after eval", f_prime)

print("############################################################")

print("Classification rate on training set", accuracy_train*100, "%")
print("Classification rate on testing set", accuracy_test*100, "%")
print("Time for finding KKT point", training_computing_time, "s")
print("Number of optimization iterations", iter)
print("Value of gamma null", "### Value of p", p_exp)

print("############################################################")

print("Difference between m(alpha) and M(alpha): ", m - M)
print("Value of q", q2*2)